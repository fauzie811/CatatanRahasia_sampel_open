package gulajava.catatanrahasia;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;

/**
 * Created by Gulajava Ministudio on 1/31/15.
 */


public class Tentangs extends ActionBarActivity {


    private Toolbar toolbar;
    private ActionBar actionbarcompat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tentangaplikasi);
        munculMenuAction(Tentangs.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            Tentangs.this.setSupportActionBar(toolbar);
        }

        actionbarcompat = Tentangs.this.getSupportActionBar();
        actionbarcompat.setDisplayHomeAsUpEnabled(true);
        actionbarcompat.setTitle(R.string.judultentang);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                Tentangs.this.finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //MENAMPILKAN MENU ACTION BAR
    private void munculMenuAction(Context context) {

        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKey = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKey != null) {
                menuKey.setAccessible(true);
                menuKey.setBoolean(config, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
