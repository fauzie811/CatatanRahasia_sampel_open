package gulajava.catatanrahasia;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Gulajava Ministudio on 1/30/15.
 */
public class Parsers {


    //SETEL TANGGAL dd/MM/yyyy
    public String setelTanggal(String strkodetanggal) {

        long tanggaltime = Long.valueOf(strkodetanggal);

        Calendar cal = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        cal.setTimeInMillis(tanggaltime);

        Date date = cal.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

        return sdf.format(date);
    }


    //SETEL TANGGAL DALAM MS
    public long setelTanggalMilis(int tahun, int bulan, int tanggalhari, int jam, int menit, int detik) {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());

        cal.set(tahun, bulan, tanggalhari, jam, menit, detik);

        Date date = cal.getTime();

        //dalam miliseken
        long msdate = date.getTime();

        Log.w("TANGGAL MILISEKEN", "" + msdate);

        return msdate;
    }


}
