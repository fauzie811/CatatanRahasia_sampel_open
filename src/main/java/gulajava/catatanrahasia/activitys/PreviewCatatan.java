package gulajava.catatanrahasia.activitys;

/**
 * Created by Gulajava Ministudio on 1/30/15.
 */


import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;

import gulajava.catatanrahasia.Konstans;
import gulajava.catatanrahasia.Parsers;
import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.dialogs.DialogHapusDataPreview;
import gulajava.catatanrahasia.modeldb.CatatanDb;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class PreviewCatatan extends ActionBarActivity {


    private Toolbar toolbars;
    private ActionBar actionbarcompat;



    //data dari db
    private String namacatatan, isicatatan, tanggalcatatan, passwordcatatan, isKunciCat;
    private int posisiklikpilih = 0;
    private TextView teksnamacatatan, teksisicatatan;
    private Bundle bundel;


    //data realm
    private Realm realm;
    private RealmQuery<CatatanDb> realmqueryCatatan;
    private RealmResults<CatatanDb> realmresultCatatan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.previewcatatan);
        munculMenuAction(PreviewCatatan.this);

        bundel = PreviewCatatan.this.getIntent().getExtras();
        posisiklikpilih = bundel.getInt(Konstans.TAG_INTENT_PILIHANKLIK);


        toolbars = (Toolbar) findViewById(R.id.toolbar);

        if (toolbars != null) {

            PreviewCatatan.this.setSupportActionBar(toolbars);

        }

        actionbarcompat = PreviewCatatan.this.getSupportActionBar();
        actionbarcompat.setDisplayHomeAsUpEnabled(true);
        actionbarcompat.setTitle(R.string.preview_judulhalaman);

        teksnamacatatan = (TextView) findViewById(R.id.teksjudulcatatan);
        teksisicatatan = (TextView) findViewById(R.id.teks_isicatatan);

        realm = Realm.getInstance(PreviewCatatan.this);


//        ambilDataCocokKlik();
    }


    @Override
    protected void onResume() {
        super.onResume();

        ambilDataCocokKlik();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        realm.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        PreviewCatatan.this.getMenuInflater().inflate(R.menu.menu_previewcat, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                PreviewCatatan.this.finish();

                return true;

            case R.id.action_hapus:

                FragmentTransaction fts = PreviewCatatan.this.getFragmentManager().beginTransaction();
                DialogHapusDataPreview dialogprev = new DialogHapusDataPreview();
                dialogprev.show(fts, "dialog catatan hapus");

                return true;

            case R.id.action_sunting:

                pindahDataSunting();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //AMBIL DATA DARI DB REALM SESUAI DENGAN POSISI KLIK LISTVIEW SEBELUMNYA
    private void ambilDataCocokKlik() {

        realmqueryCatatan = realm.where(CatatanDb.class);
        realmresultCatatan = realmqueryCatatan.findAll();

        if (realmresultCatatan.size() > 0) {

            CatatanDb catatandbs = realmresultCatatan.get(posisiklikpilih);

            namacatatan = catatandbs.getNamacatatan();
            isicatatan = catatandbs.getIsicatatan();
            tanggalcatatan = catatandbs.getTanggalcatatan();
            passwordcatatan = catatandbs.getPasswordcatatan();
            isKunciCat = catatandbs.getIsDikunci();

            teksnamacatatan.setText(namacatatan);
            teksisicatatan.setText(isicatatan);

            Parsers parsers = new Parsers();
            String tanggalddmmyy = parsers.setelTanggal(tanggalcatatan);
            actionbarcompat.setSubtitle(tanggalddmmyy);
        }

    }


    //HAPUS DATA DB REALMS
    public void hapusDbRealms() {

        try {

            realm.beginTransaction();

            CatatanDb catdbhapus = realmresultCatatan.get(posisiklikpilih);
            catdbhapus.removeFromRealm();

            realm.commitTransaction();

            Toast.makeText(PreviewCatatan.this,R.string.toasthapusdetil,Toast.LENGTH_SHORT).show();
            PreviewCatatan.this.finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }



    }


    //PINDAH DATA SUNTING
    private void pindahDataSunting() {

        Intent intentsunting = new Intent(PreviewCatatan.this, SuntingCatatan.class);
        bundel.putString(Konstans.TAG_INTENT_JUDULCATATAN, namacatatan);
        bundel.putString(Konstans.TAG_INTENT_ISICATATAN, isicatatan);
        bundel.putString(Konstans.TAG_INTENT_KATAKUNCI, passwordcatatan);
        bundel.putString(Konstans.TAG_INTENT_ISKUNCI, isKunciCat);

        intentsunting.putExtras(bundel);
        PreviewCatatan.this.startActivity(intentsunting);

    }


    //MENAMPILKAN MENU ACTION BAR
    private void munculMenuAction(Context context) {

        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKey = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKey != null) {
                menuKey.setAccessible(true);
                menuKey.setBoolean(config, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
