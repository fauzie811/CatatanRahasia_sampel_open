package gulajava.catatanrahasia.activitys;

/**
 * Created by Gulajava Ministudio on 1/30/15.
 */


import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.Calendar;

import gulajava.catatanrahasia.Konstans;
import gulajava.catatanrahasia.Parsers;
import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.dialogs.DialogPasswordEdit;
import gulajava.catatanrahasia.modeldb.CatatanDb;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class SuntingCatatan extends ActionBarActivity {


    private Toolbar toolbars;
    private ActionBar actionbarcompat;
    private View viewhides;


    //database realms
    private Realm realms;
    private RealmQuery<CatatanDb> realmqueryCatatan;
    private RealmResults<CatatanDb> realmresultCatatan;

    private EditText editjudul, editisi;
    private String strjudul = "";
    private String strisi = "";
    private String strpassword = "";
    private String isPassword = Konstans.TAG_FALSE;

    private TextView teks_katasandisetel;

    private Calendar calcatatan;
    private int tahun;
    private int bulan;
    private int tanggal;
    private int jam;
    private int menit;
    private int detik;

    private Parsers parsers;


    //data dari halaman preview
    private Bundle bundel;
    private int posisicatatanDB = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tambahcatatan);
        munculMenuAction(SuntingCatatan.this);

        bundel = SuntingCatatan.this.getIntent().getExtras();

        strjudul = bundel.getString(Konstans.TAG_INTENT_JUDULCATATAN);
        strisi = bundel.getString(Konstans.TAG_INTENT_ISICATATAN);
        strpassword = bundel.getString(Konstans.TAG_INTENT_KATAKUNCI);
        isPassword = bundel.getString(Konstans.TAG_INTENT_ISKUNCI);
        posisicatatanDB = bundel.getInt(Konstans.TAG_INTENT_PILIHANKLIK);


        toolbars = (Toolbar) findViewById(R.id.toolbar);
        if (toolbars != null) {

            SuntingCatatan.this.setSupportActionBar(toolbars);

        }

        actionbarcompat = SuntingCatatan.this.getSupportActionBar();
        actionbarcompat.setDisplayHomeAsUpEnabled(true);
        actionbarcompat.setTitle(R.string.sunting_judultoolbar);
        viewhides = findViewById(R.id.viewshide);

        /** inisialisasi realm **/
        realms = Realm.getInstance(SuntingCatatan.this);

        editjudul = (EditText) findViewById(R.id.edit_judulcatatan);
        editisi = (EditText) findViewById(R.id.edit_isicatatan);

        teks_katasandisetel = (TextView) findViewById(R.id.teks_ingatkatasandi);
        teks_katasandisetel.setVisibility(View.GONE);

        calcatatan = Calendar.getInstance();
        tahun = calcatatan.get(Calendar.YEAR);
        bulan = calcatatan.get(Calendar.MONTH);
        tanggal = calcatatan.get(Calendar.DAY_OF_MONTH);
        jam = calcatatan.get(Calendar.HOUR_OF_DAY);
        menit = calcatatan.get(Calendar.MINUTE);
        detik = calcatatan.get(Calendar.MILLISECOND);

        parsers = new Parsers();

        //isi data
        isiDataSebelumnya();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        //tutup realm untuk aktivitas halaman ini
        realms.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        SuntingCatatan.this.getMenuInflater().inflate(R.menu.menu_tambahcat, menu);

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                hilangKeyboard(SuntingCatatan.this, viewhides);
                SuntingCatatan.this.finish();

                return true;

            case R.id.action_simpan:

                hilangKeyboard(SuntingCatatan.this, viewhides);
                ambilDataUpdate();

                return true;

            case R.id.action_tambahpass:

                setelPasswordDialog();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //SEMBUNYIKAN KEYBOARD
    private void hilangKeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


    //MENAMPILKAN MENU ACTION BAR
    private void munculMenuAction(Context context) {

        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKey = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKey != null) {
                menuKey.setAccessible(true);
                menuKey.setBoolean(config, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //ISI DATA SEBELUMNYA
    private void isiDataSebelumnya() {

        editjudul.setText(strjudul);
        editisi.setText(strisi);

        if (isPassword.contentEquals(Konstans.TAG_TRUE) && strpassword.length() > 0) {

            teks_katasandisetel.setVisibility(View.VISIBLE);

        } else {
            teks_katasandisetel.setVisibility(View.GONE);
        }

    }


    //SETEL PASSWORD DARI DIALOG
    private void setelPasswordDialog() {

        Bundle bundeldatapass = new Bundle();
        bundeldatapass.putString(Konstans.TAG_INTENT_KATAKUNCI, strpassword);

        DialogPasswordEdit dialogpass = new DialogPasswordEdit();
        dialogpass.setArguments(bundeldatapass);
        FragmentTransaction fts = SuntingCatatan.this.getFragmentManager().beginTransaction();
        dialogpass.show(fts, "dialog password edit");

    }


    public void ambilPassword(String katakunci, String katakunciulang) {

        hilangKeyboard(SuntingCatatan.this, viewhides);

        if (katakunci.length() > 3 && katakunciulang.length() > 3 && katakunci.contentEquals(katakunciulang)) {

            this.strpassword = katakunci;
            this.isPassword = Konstans.TAG_TRUE;
            Toast.makeText(SuntingCatatan.this, R.string.toast_sukseskatakunci, Toast.LENGTH_SHORT).show();
            teks_katasandisetel.setVisibility(View.VISIBLE);

        } else {

            this.strpassword = "";
            this.isPassword = Konstans.TAG_FALSE;
            Toast.makeText(SuntingCatatan.this, R.string.toast_gagalkatakunci, Toast.LENGTH_SHORT).show();
            teks_katasandisetel.setVisibility(View.GONE);
        }

    }


    //AMBIL DATA UNTUK UPDATE
    private void ambilDataUpdate() {

        strjudul = editjudul.getText().toString();
        strisi = editisi.getText().toString();

        boolean isValidasi = cekValidasi();

        if (isValidasi) {

            Toast.makeText(SuntingCatatan.this, R.string.toast_simpan, Toast.LENGTH_SHORT).show();

            long tanggalformatms = parsers.setelTanggalMilis(tahun, bulan, tanggal, jam, menit, detik);

            /** tulis ulang data catatan ke dalam database dengan metode realm **/

            realmqueryCatatan = realms.where(CatatanDb.class);
            realmresultCatatan = realmqueryCatatan.findAll();

            CatatanDb catatanDB = realmresultCatatan.get(posisicatatanDB);


            realms.beginTransaction();

            catatanDB.setTanggalcatatan("" + tanggalformatms);
            catatanDB.setNamacatatan(strjudul);
            catatanDB.setIsicatatan(strisi);
            catatanDB.setPasswordcatatan(strpassword);
            catatanDB.setIsDikunci(isPassword);

            realms.commitTransaction();

            SuntingCatatan.this.finish();
        }
    }


    //VALIDASI ISI DATA CATATAN
    private boolean cekValidasi() {

        boolean hasilCek = false;

        if (strjudul.length() < 3) {
            tampilToast(editjudul, R.string.toast_gagaljudul);
        } else if (strisi.length() < 3) {
            tampilToast(editisi, R.string.toast_gagalisi);
        } else {
            hasilCek = true;
        }

        return hasilCek;
    }

    //TAMPILKAN TOAST
    private void tampilToast(EditText edits, int resPesan) {
        Toast.makeText(SuntingCatatan.this, resPesan, Toast.LENGTH_SHORT).show();
        edits.requestFocus();
    }


}
