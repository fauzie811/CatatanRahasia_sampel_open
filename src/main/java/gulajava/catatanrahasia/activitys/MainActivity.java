package gulajava.catatanrahasia.activitys;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;

import gulajava.catatanrahasia.Holders;
import gulajava.catatanrahasia.Konstans;
import gulajava.catatanrahasia.Parsers;
import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.Tentangs;
import gulajava.catatanrahasia.dialogs.DialogHapusAllData;
import gulajava.catatanrahasia.dialogs.DialogHapusCentangData;
import gulajava.catatanrahasia.dialogs.DialogMasukPassword;
import gulajava.catatanrahasia.modeldb.CatatanDb;
import io.realm.Realm;
import io.realm.RealmBaseAdapter;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class MainActivity extends ActionBarActivity {


    private Toolbar toolbars;
    private ActionBar actionbarcompat;


    //data realm
    private Realm realm;
    private RealmQuery<CatatanDb> realmqueryCatatan;
    private RealmResults<CatatanDb> realmresultCatatan;

    private Intent intents;
    private LayoutInflater inflaterlist;

    //data dari db
    private String namacatatan, isicatatan, statuspasswords, tanggalcatatan;
    private String isipasswords;
    private ListView listviewcatatan;


    private Bundle bundel;

    private Parsers parsers;

    private FragmentTransaction fts;


    //HAPUS DATA YANG DICENTANG DENGAN REALM
    private boolean isDataAda = false;
    private boolean isHapusSatu = false;
    private static final int ID_HAPUSSATU = 34;
    private static final int ID_HAPUSEMUA = 35;


    private CheckBox cekbokhapus;
    private ArrayList<Boolean> arrItemCeks;
    private View viewumpan;
    private TextView tekstanpadata;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuawal);
        munculMenuAction(MainActivity.this);

        toolbars = (Toolbar) findViewById(R.id.toolbar);

        if (toolbars != null) {

            MainActivity.this.setSupportActionBar(toolbars);

        }

        actionbarcompat = MainActivity.this.getSupportActionBar();
        actionbarcompat.setTitle(R.string.app_name);
        actionbarcompat.setSubtitle(R.string.app_namesub);

        /** inisialisasi realms **/
        realm = Realm.getInstance(MainActivity.this);
        parsers = new Parsers();

        listviewcatatan = (ListView) findViewById(R.id.list_daftarcatatan);
        viewumpan = findViewById(R.id.viewumpan);
        tekstanpadata = (TextView) findViewById(R.id.tekstanpadata);
        MainActivity.this.registerForContextMenu(viewumpan);

        ambilDataDbRealm();

    }


    @Override
    protected void onResume() {
        super.onResume();

        try {

            muatUlangListViewMenu();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        //tutup realm untuk aktivitas halaman ini
        realm.close();

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem menuitemtambah = menu.findItem(R.id.action_tambahdata);
        MenuItem menuitemhapusatu = menu.findItem(R.id.action_hapusatu);
        MenuItem menuitemhapusatubatal = menu.findItem(R.id.action_hapusatubatal);
        MenuItem menuitemhapusmenu = menu.findItem(R.id.action_hapus);

        MenuItem menutentangapps = menu.findItem(R.id.action_tentangapp);

        if (isDataAda) {

            if (isHapusSatu) {
                menuitemtambah.setEnabled(false).setVisible(false);
                menuitemhapusatu.setEnabled(true).setVisible(true);
                menuitemhapusatubatal.setEnabled(true).setVisible(true);
                menuitemhapusmenu.setEnabled(false).setVisible(false);
                menutentangapps.setEnabled(false).setVisible(false);
            } else {
                menuitemtambah.setEnabled(true).setVisible(true);
                menuitemhapusatu.setEnabled(false).setVisible(false);
                menuitemhapusatubatal.setEnabled(false).setVisible(false);
                menuitemhapusmenu.setEnabled(true).setVisible(true);
                menutentangapps.setEnabled(true).setVisible(true);
            }

        } else {
            menuitemtambah.setEnabled(true).setVisible(true);
            menuitemhapusatu.setEnabled(false).setVisible(false);
            menuitemhapusatubatal.setEnabled(false).setVisible(false);
            menuitemhapusmenu.setEnabled(false).setVisible(false);
            menutentangapps.setEnabled(true).setVisible(true);
        }


        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case R.id.action_tambahdata:

                intents = new Intent(MainActivity.this, TambahCatatan.class);
                MainActivity.this.startActivity(intents);

                return true;

            case R.id.action_hapusatu:

                //dialog hapus centang
                FragmentTransaction ftcentang = MainActivity.this.getFragmentManager().beginTransaction();
                DialogHapusCentangData dialoghapuscentangs = new DialogHapusCentangData();
                dialoghapuscentangs.show(ftcentang, "dialog hapus semua data");

                return true;

            case R.id.action_hapusatubatal:

                isHapusSatu = false;
                muatUlangListViewMenu();

                return true;

            case R.id.action_hapus:

                MainActivity.this.openContextMenu(viewumpan);

                return true;

            case R.id.action_tentangapp:

                intents = new Intent(MainActivity.this, Tentangs.class);
                MainActivity.this.startActivity(intents);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(ContextMenu.NONE, ID_HAPUSSATU, ContextMenu.NONE, R.string.konteksmenu_hapusatu);
        menu.add(ContextMenu.NONE, ID_HAPUSEMUA, ContextMenu.NONE, R.string.konteksmenu_hapusemua);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case ID_HAPUSSATU:

                isHapusSatu = true;
                muatUlangListHapusSatu();

                return true;

            case ID_HAPUSEMUA:

                //dialog hapus semua data realm
                FragmentTransaction ft = MainActivity.this.getFragmentManager().beginTransaction();
                DialogHapusAllData dialoghapusdata = new DialogHapusAllData();
                dialoghapusdata.show(ft, "dialog hapus semua data");

                return true;
        }

        return super.onContextItemSelected(item);
    }


    //MENAMPILKAN MENU ACTION BAR
    private void munculMenuAction(Context context) {

        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKey = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKey != null) {
                menuKey.setAccessible(true);
                menuKey.setBoolean(config, false);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //AMBIL DATA DARI DB
    private void ambilDataDbRealm() {

        realmqueryCatatan = realm.where(CatatanDb.class);
        realmresultCatatan = realmqueryCatatan.findAll();

        muatUlangListViewMenu();

    }


    //ADAPTER KE LISTVIEW
    public class AdapterRealmsList extends RealmBaseAdapter<CatatanDb> implements ListAdapter {


        public AdapterRealmsList(Context context, RealmResults<CatatanDb> realmResults,
                                 boolean automaticUpdate) {
            super(context, realmResults, automaticUpdate);

            arrItemCeks = new ArrayList<>();
            int panjangkursor = realmResults.size();

            //INISIALISASI ARRAY DICENTANG
            for (int i = 0; i < panjangkursor; i++) {
                arrItemCeks.add(i, false);
            }
        }


        public View getView(int i, View view, ViewGroup viewGroup) {

            Holders holders;

            if (view == null) {

                inflaterlist = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflaterlist.inflate(R.layout.desainlist_catatan, viewGroup, false);
                holders = new Holders(view);
                view.setTag(holders);
            } else {
                holders = (Holders) view.getTag();
            }

            try {

                CatatanDb catdb = realmresultCatatan.get(i);
                namacatatan = catdb.getNamacatatan();
                isicatatan = catdb.getIsicatatan();
                statuspasswords = catdb.getIsDikunci();
                tanggalcatatan = parsers.setelTanggal(catdb.getTanggalcatatan());

                holders.getTeksjudulcatatan().setText(namacatatan);
                holders.getTekstanggal().setText(tanggalcatatan);


                if (statuspasswords.contentEquals(Konstans.TAG_TRUE)) {
                    holders.getGambargembok().setImageDrawable(MainActivity.this.getResources().getDrawable(R.drawable.ic_terkunci_gembok));
                    holders.getTeksisicatatan().setText("(isi catatan dirahasiakan)");
                } else {
                    holders.getGambargembok().setImageDrawable(MainActivity.this.getResources().getDrawable(R.drawable.ic_terkunci_ungembok));
                    holders.getTeksisicatatan().setText(isicatatan);
                }

                cekbokhapus = holders.getCekhapuscentang();

                if (isHapusSatu) {
                    cekbokhapus.setTag(i);
                    cekbokhapus.setOnClickListener(listenercekbok);
                    cekbokhapus.setVisibility(View.VISIBLE);
                    cekbokhapus.setChecked(arrItemCeks.get(i));
                } else {
                    cekbokhapus.setVisibility(View.GONE);
                    cekbokhapus.setChecked(false);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return view;
        }
    }


    //LISTENER CHECK BOX HAPUS SATU SATU
    View.OnClickListener listenercekbok = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            CheckBox cbcentang = (CheckBox) view.findViewById(R.id.centang_hapusatu);
            int intposisicentang = Integer.valueOf(cbcentang.getTag().toString() + "");

            if (cbcentang.isChecked()) {
                arrItemCeks.set(intposisicentang, true);
            } else {
                arrItemCeks.set(intposisicentang, false);
            }
        }
    };


    //HAPUS DATABASE CATATAN YANG SUDAH DITANDAI
    public void hapusCentangDataRealm() {

        int panjangcentang = arrItemCeks.size();
        boolean isCentang = false;

        ArrayList<Integer> posisihapuslist = new ArrayList<>();

        for (int i = 0; i < panjangcentang; i++) {

            isCentang = arrItemCeks.get(i);

            if (isCentang) {

                //ambil posisi centang yang nilainya true
                posisihapuslist.add(i);

            }
        }

        try {
            //hapus dengan menggunakan realm, berdasarkan posisi di array realm results
            //posisi di realm result ini akan menjadi patokan untuk mendapatkan objek kelas CatatanDB Realm
            realm.beginTransaction();

            for (int idposisi : posisihapuslist) {

                CatatanDb catdbhapuscentang = realmresultCatatan.get(idposisi);
                catdbhapuscentang.removeFromRealm();

            }

            realm.commitTransaction();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        isHapusSatu = false;
        muatUlangListViewMenu();

        Toast.makeText(MainActivity.this, R.string.toasthapuspilih, Toast.LENGTH_SHORT).show();
    }


    //HAPUS SEMUA DATA
    public void hapusSemuaDataRealm() {

        realm.beginTransaction();

        realmresultCatatan.clear();

        realm.commitTransaction();

        isHapusSatu = false;
        muatUlangListViewMenu();

        Toast.makeText(MainActivity.this, R.string.toasthapusemua, Toast.LENGTH_SHORT).show();
    }


    //KETIKA LISTVIEW DI KLIK
    AdapterView.OnItemClickListener listenerlist = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            try {

                CatatanDb catatdbklik = realmresultCatatan.get(i);

                statuspasswords = catatdbklik.getIsDikunci();
                isipasswords = catatdbklik.getPasswordcatatan();

                bundel = new Bundle();
                bundel.putInt(Konstans.TAG_INTENT_PILIHANKLIK, i);
                intents = new Intent(MainActivity.this, PreviewCatatan.class);
                intents.putExtras(bundel);

                if (statuspasswords.contentEquals(Konstans.TAG_TRUE)) {
                    tampilDialogIsiPass();
                } else {
                    MainActivity.this.startActivity(intents);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    };


    //KETIKA LISTVIEW DALAM MODE HAPUS DATA SATU SATU
    AdapterView.OnItemClickListener listenerlistmodeHapus = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            //KOSONG
        }
    };


    //TAMPILKAN DIALOG
    private void tampilDialogIsiPass() {

        DialogMasukPassword dialogmasukpass = new DialogMasukPassword();
        fts = MainActivity.this.getFragmentManager().beginTransaction();
        dialogmasukpass.show(fts, "dialog masukkan kata sandi");

    }


    //PINDAH KE HALAMAN BERIKUTNYA
    public void pindahHalamanKlikList(String passwordmasuk) {

        if (passwordmasuk.contentEquals(isipasswords)) {

            MainActivity.this.startActivity(intents);

        } else {
            Toast.makeText(MainActivity.this, R.string.dialogpass_lihat_toastgagal, Toast.LENGTH_SHORT).show();
        }

    }


    //SET ADAPTER DAN MUAT ULANG
    private void muatUlangListViewMenu() {

        realm.refresh();

        if (realmresultCatatan.size() > 0) {
            isDataAda = true;
            tekstanpadata.setVisibility(View.GONE);
            listviewcatatan.setAdapter(new AdapterRealmsList(MainActivity.this, realmresultCatatan, true));
            listviewcatatan.setOnItemClickListener(listenerlist);
            listviewcatatan.setVisibility(View.VISIBLE);
        } else {
            isDataAda = false;
            tekstanpadata.setVisibility(View.VISIBLE);
            listviewcatatan.setVisibility(View.GONE);
        }

        MainActivity.this.invalidateOptionsMenu();
    }


    //SET ADAPTER UNTUK HAPUS SATU SATU
    private void muatUlangListHapusSatu() {

        realm.refresh();
        MainActivity.this.invalidateOptionsMenu();

        if (realmresultCatatan.size() > 0) {

            tekstanpadata.setVisibility(View.GONE);
            listviewcatatan.setAdapter(new AdapterRealmsList(MainActivity.this, realmresultCatatan, true));
            listviewcatatan.setOnItemClickListener(listenerlistmodeHapus);
            listviewcatatan.setVisibility(View.VISIBLE);
        } else {
            tekstanpadata.setVisibility(View.VISIBLE);
            listviewcatatan.setVisibility(View.GONE);
        }

    }


}
