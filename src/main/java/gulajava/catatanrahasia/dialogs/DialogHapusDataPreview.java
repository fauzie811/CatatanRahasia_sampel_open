package gulajava.catatanrahasia.dialogs;

/**
 * Created by Gulajava Ministudio on 1/31/15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.activitys.PreviewCatatan;


public class DialogHapusDataPreview extends DialogFragment {


    private PreviewCatatan prevactss;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        prevactss = (PreviewCatatan) DialogHapusDataPreview.this.getActivity();
        DialogHapusDataPreview.this.setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        AlertDialog.Builder builderdialog = new AlertDialog.Builder(DialogHapusDataPreview.this.getActivity());
        builderdialog.setMessage(R.string.dialogcatini_semua);
        builderdialog.setPositiveButton(R.string.dialogpass_lihat_oke, listenerya);
        builderdialog.setNegativeButton(R.string.dialogpass_lihat_batal, listenerno);


        return builderdialog.create();
    }


    DialogInterface.OnClickListener listenerya = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            prevactss.hapusDbRealms();
            DialogHapusDataPreview.this.getDialog().dismiss();


        }
    };

    DialogInterface.OnClickListener listenerno = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            DialogHapusDataPreview.this.getDialog().dismiss();
        }
    };


}
