package gulajava.catatanrahasia.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.activitys.MainActivity;

/**
 * Created by Gulajava Ministudio on 1/30/15.
 */


public class DialogMasukPassword extends DialogFragment {


    private MainActivity mainAct;
    private EditText editkatasandi;

    private String strkatasandi = "";






    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        mainAct = (MainActivity) DialogMasukPassword.this.getActivity();
        DialogMasukPassword.this.setStyle(DialogFragment.STYLE_NO_TITLE,0);

        AlertDialog.Builder builderdialog = new AlertDialog.Builder(DialogMasukPassword.this.getActivity());
        LayoutInflater inflaterdialog = (LayoutInflater) DialogMasukPassword.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View views = inflaterdialog.inflate(R.layout.dialogpasswordliatcat,null);

        editkatasandi = (EditText) views.findViewById(R.id.edit_katasandi);

        builderdialog.setView(views);
        builderdialog.setPositiveButton(R.string.dialogpass_lihat_oke,listeneroke);
        builderdialog.setNegativeButton(R.string.dialogpass_lihat_batal,listenerbatal);


        return builderdialog.create();
    }





    DialogInterface.OnClickListener listeneroke = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            hilangKeyboard(DialogMasukPassword.this.getActivity(),editkatasandi);

            strkatasandi = editkatasandi.getText().toString();
            mainAct.pindahHalamanKlikList(strkatasandi);

            DialogMasukPassword.this.getDialog().dismiss();

        }
    };


    DialogInterface.OnClickListener listenerbatal = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            hilangKeyboard(DialogMasukPassword.this.getActivity(),editkatasandi);
            DialogMasukPassword.this.getDialog().dismiss();
        }
    };



    //SEMBUNYIKAN KEYBOARD
    private void hilangKeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }














}
