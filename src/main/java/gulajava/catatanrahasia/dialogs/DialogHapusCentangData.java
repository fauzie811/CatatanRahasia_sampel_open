package gulajava.catatanrahasia.dialogs;

/**
 * Created by Gulajava Ministudio on 1/31/15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.activitys.MainActivity;


public class DialogHapusCentangData extends DialogFragment {


    private MainActivity mainact;





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        mainact = (MainActivity) DialogHapusCentangData.this.getActivity();
        DialogHapusCentangData.this.setStyle(DialogFragment.STYLE_NO_TITLE,0);

        AlertDialog.Builder builderdialog = new AlertDialog.Builder(DialogHapusCentangData.this.getActivity());
        builderdialog.setMessage(R.string.dialoghapus_centang);
        builderdialog.setPositiveButton(R.string.dialogpass_lihat_oke,listenerya);
        builderdialog.setNegativeButton(R.string.dialogpass_lihat_batal,listenerno);


        return builderdialog.create();
    }


    DialogInterface.OnClickListener listenerya = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            mainact.hapusCentangDataRealm();
            DialogHapusCentangData.this.getDialog().dismiss();


        }
    };

    DialogInterface.OnClickListener listenerno = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            DialogHapusCentangData.this.getDialog().dismiss();
        }
    };




}
