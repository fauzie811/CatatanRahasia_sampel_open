package gulajava.catatanrahasia.dialogs;

/**
 * Created by Gulajava Ministudio on 1/30/15.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import gulajava.catatanrahasia.Konstans;
import gulajava.catatanrahasia.R;
import gulajava.catatanrahasia.activitys.TambahCatatan;


public class DialogPassword extends DialogFragment {


    private TambahCatatan tambahAct;
    private EditText editpass, editpassulang;

    private String katakuncidariact = "";
    private Bundle bundels;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        tambahAct = (TambahCatatan) DialogPassword.this.getActivity();
        DialogPassword.this.setStyle(DialogFragment.STYLE_NO_TITLE, 0);

        bundels = DialogPassword.this.getArguments();
        katakuncidariact = bundels.getString(Konstans.TAG_INTENT_KATAKUNCI);


        AlertDialog.Builder builderdialog = new AlertDialog.Builder(DialogPassword.this.getActivity());

        LayoutInflater inflaters = (LayoutInflater) DialogPassword.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View views = inflaters.inflate(R.layout.dialogpassword, null);

        editpass = (EditText) views.findViewById(R.id.edit_passawal);
        editpassulang = (EditText) views.findViewById(R.id.edit_passulang);
        editpass.setText(katakuncidariact);
        editpassulang.setText(katakuncidariact);


        builderdialog.setView(views);
        builderdialog.setPositiveButton(R.string.tekssetel, listenersimpan);
        builderdialog.setNegativeButton(R.string.tekssetelbatal, listenerbatal);

        return builderdialog.create();
    }


    DialogInterface.OnClickListener listenersimpan = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            hilangKeyboard(DialogPassword.this.getActivity(), editpassulang);

            String strpass = editpass.getText().toString();
            String strpassulang = editpassulang.getText().toString();

            tambahAct.ambilPassword(strpass, strpassulang);
            DialogPassword.this.getDialog().dismiss();

        }
    };


    DialogInterface.OnClickListener listenerbatal = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            hilangKeyboard(DialogPassword.this.getActivity(), editpassulang);
            DialogPassword.this.getDialog().dismiss();
        }
    };


    //SEMBUNYIKAN KEYBOARD
    private void hilangKeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


}
