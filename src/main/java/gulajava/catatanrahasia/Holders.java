package gulajava.catatanrahasia;

/**
 * Created by Gulajava Ministudio on 1/29/15.
 */

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


public class Holders {


    View view;

    private TextView teksjudulcatatan = null;
    private TextView teksisicatatan = null;
    private ImageView gambargembok = null;
    private TextView tekstanggal = null;

    private CheckBox cekhapuscentang = null;




    public Holders(View base) {
        this.view = base;
    }


    public TextView getTeksjudulcatatan() {

        if (teksjudulcatatan == null) {
            teksjudulcatatan = (TextView) view.findViewById(R.id.teksjudulcatatan);
        }

        return teksjudulcatatan;
    }

    public TextView getTeksisicatatan() {

        if (teksisicatatan == null) {
            teksisicatatan = (TextView) view.findViewById(R.id.teksisijudulcatatan);
        }

        return teksisicatatan;
    }

    public ImageView getGambargembok() {

        if (gambargembok == null) {
            gambargembok = (ImageView) view.findViewById(R.id.gambarstatus);
        }

        return gambargembok;
    }


    public TextView getTekstanggal() {

        if (tekstanggal == null) {
            tekstanggal = (TextView) view.findViewById(R.id.teks_tanggalcatatan);
        }

        return tekstanggal;
    }


    public CheckBox getCekhapuscentang() {

        if (cekhapuscentang == null) {
            cekhapuscentang = (CheckBox) view.findViewById(R.id.centang_hapusatu);
        }

        return cekhapuscentang;
    }
}
