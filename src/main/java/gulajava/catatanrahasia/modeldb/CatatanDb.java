package gulajava.catatanrahasia.modeldb;

import io.realm.RealmObject;

/**
 * Created by Gulajava Ministudio on 1/29/15.
 */


public class CatatanDb extends RealmObject {


    //kolom database dibuat dengan sistem POJO dan Getter Setter
    private String tanggalcatatan;
    private String namacatatan;
    private String isicatatan;
    private String passwordcatatan;
    private String isDikunci;

    public String getTanggalcatatan() {
        return tanggalcatatan;
    }

    public void setTanggalcatatan(String tanggalcatatan) {
        this.tanggalcatatan = tanggalcatatan;
    }

    public String getNamacatatan() {
        return namacatatan;
    }

    public void setNamacatatan(String namacatatan) {
        this.namacatatan = namacatatan;
    }

    public String getIsicatatan() {
        return isicatatan;
    }

    public void setIsicatatan(String isicatatan) {
        this.isicatatan = isicatatan;
    }

    public String getPasswordcatatan() {
        return passwordcatatan;
    }

    public void setPasswordcatatan(String passwordcatatan) {
        this.passwordcatatan = passwordcatatan;
    }

    public String getIsDikunci() {
        return isDikunci;
    }

    public void setIsDikunci(String isDikunci) {
        this.isDikunci = isDikunci;
    }
}
