package gulajava.catatanrahasia;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import android.os.Handler;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import gulajava.catatanrahasia.activitys.MainActivity;

/**
 * Created by Gulajava Ministudio on 1/31/15.
 */




public class Splashers extends ActionBarActivity {


    private Intent intents;
    private Runnable splsh = new Runnable() {
        @Override
        public void run() {

            Splashers.this.startActivity(intents);
            Splashers.this.finish();

        }
    };

    private Handler handlers;

    private ImageView gambarsplashers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashs);

        gambarsplashers = (ImageView) findViewById(R.id.gambarsplashs);
        Picasso.with(Splashers.this).load(R.drawable.splash).into(gambarsplashers);

        intents = new Intent(Splashers.this, MainActivity.class);

        handlers = new Handler();
        handlers.postDelayed(splsh,1000);


    }


}
