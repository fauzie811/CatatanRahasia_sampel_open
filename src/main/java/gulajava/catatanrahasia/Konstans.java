package gulajava.catatanrahasia;

/**
 * Created by Gulajava Ministudio on 1/29/15.
 */
public class Konstans {


    public static final String TAG_TRUE = "true";
    public static final String TAG_FALSE = "false";

    public static final String TAG_INTENT_KATAKUNCI = "katakuncibawa";

    public static final String TAG_INTENT_JUDULCATATAN = "judulcatatan";
    public static final String TAG_INTENT_ISICATATAN = "isicatatan";
    public static final String TAG_INTENT_PILIHANKLIK = "pilihanklikcatat";
    public static final String TAG_INTENT_ISKUNCI = "statusKunci";


}
